# M&C Cheat Sheets series

Authors:

- Khambar Dussaliyev

Reviewer:

- Adina Magavina

## Document description

This cheat sheets series are prepared to help M&C students to have a quick reference materials from classroom work.  
There is a lot of materials devoted to OOP in general and Java in particular. I have no intention to add a lesser one to the pile.
Nevertheless reference materials could drastically increase students' work quality and help them in preparing to lessons.  

Main objective of this cheat sheet series is not to be complete and full dive-in into the topic, but rather be a quick reference material to explain classroom work and help in preparing for the next topics.  

Topics listed here are aligned with education process for OOP students in M&C education center. Although they are tied to specific topic coverage, they may be used without tight bind to education process.

Although OOP is primary focus of cheet series at the time being, topic coverage could be extended to different topics being teached at M&C education center.

## Legal

This document, as can be seen from [License](./license) this work is published free. Anyone can copy, distribute and modify this documents.  
Students are welcome to print/use/copy reference materials as they feel necessary.  

## Course structure

- [ ] OOP
    - [ ] java ecosystem
    - [ ] OOP paradigm
    - [ ] methods
    - [ ] static
    - [ ] constructors
    - [ ] access modifiers
    - [x] exception
    - [ ] inheritance
    - [ ] polymorphism
    - [ ] collections
    - [ ] interfaces
    - [ ] abstract classes
    - [ ] java reflections
    - [ ] multithreading
    - [ ] elements of functional programming
