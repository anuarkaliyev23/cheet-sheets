public class Rectangle extends Figure {
    private Point a;
    private Point b;
    private Point c;
    private Point d;

    public Rectangle() {
    }

    public Rectangle(Point a, Point b, Point c, Point d) {
        if (this.exists(a, b, c, d)) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
        else throw new RuntimeException("Non-existent rectangle");
    }

    public Rectangle(Rectangle other) {
        this.a = other.a;
        this.b = other.b;
        this.c = other.c;
        this.d = other.d;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        if (exists(a, this.b, this.c, this.d)) {
            this.a = a;
        } else throw new RuntimeException("Non-existent Triangle");
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        if (exists(this.a, b, this.c, this.d)) {
            this.b = b;
        } else throw new RuntimeException("Non-existent Triangle");
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        if (exists(this.a, this.b, c, this.d)) {
            this.c = c;
        } else throw new RuntimeException("Non-existent Triangle");
    }

    public Point getD() {
        return d;
    }

    public void setD(Point d) {
        if (exists(this.a, this.b, this.c, d)) {
            this.d = d;
        } else throw new RuntimeException("Non-existent Triangle");
    }

    private boolean exists(Point a, Point b, Point c, Point d) {
        return Point.length(a, c) == Point.length(b, d);
    }

    public double perimeter() {
        return Point.length(a, b) + Point.length(b, c) + Point.length(c, d) + Point.length(a, d);
    }

    public double area() {
        return Point.length(a, b) * Point.length(b, c);
    }
}
