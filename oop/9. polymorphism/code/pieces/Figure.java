/**
 * Any figure in Euclidian geometry
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class Figure {

    public double perimeter() {
        throw new RuntimeException();
    }

    public double area() {
        throw new RuntimeException();
    }
}
