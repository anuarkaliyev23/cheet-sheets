public class Main {
    public static void main(String[] args) {
        Figure triangle = new Triangle(new Point(0,0), new Point(5, 5), new Point(10, 0));
        Figure rectangle = new Rectangle(new Point(0, 0), new Point(0, 5), new Point(5, 5), new Point(5, 0));
        Figure circle = new Circle(new Point(0, 0), new Point(10, 0));
        //
        System.out.println(triangle.area());
        System.out.println(rectangle.area());
        System.out.println(circle.area());

        System.out.println(triangle);
    }
}
