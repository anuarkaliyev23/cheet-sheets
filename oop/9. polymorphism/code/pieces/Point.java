/**
 * Represents Geometric point in cartesian coordinates
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
public class Point {
    /**
     * x-coordinate on cartesian coordinate system
     *
     * @since 0.1.0
     * */
    private int x;

    /**
     * y-coordinate on cartesian coordinate system
     *
     * @since 0.1.0
     * */
    private int y;

    // Default constructor
    public Point() {}

    // Fully-parametrized constructor
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Copy-constructor
    public Point(Point other) {
        this.x = other.x;
        this.y = other.y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * Calculates difference between two points on cartesian plane
     *
     * @param first first point
     * @param second second point
     *
     * @return distance between two points
     * */
    public static double length(Point first, Point second) {
        return Math.sqrt(Math.pow(second.x - first.x, 2.0) + Math.pow(second.y - first.y, 2.0));
    }

    /**
     * Difference between this and other points on cartesian plane
     *
     * @param other destination point for length calculation
     *
     * @return distance between two points
     * */
    public double length(Point other) {
        return length(this, other);
    }

}
