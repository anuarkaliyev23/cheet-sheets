/**
 * Triangle existing in Euclidean geometry
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
public class Triangle extends Figure {
    /**
     * First vertex point
     *
     * @since 0.1.0
     * */
    private Point a;
    /**
     * Second vertex point
     *
     * @since 0.1.0
     * */
    private Point b;
    /**
     * Third vertex point
     *
     * @since 0.1.0
     * */
    private Point c;

    public Triangle() {
    }

    public Triangle(Point a, Point b, Point c) {
//        if (exists(a, b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
//        } throw new RuntimeException("Non-existent triangle");
    }

    public Triangle(Triangle other) {
        this.a = other.a;
        this.b = other.b;
        this.c = other.c;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        if (exists(a, this.b, this.c)) {
            this.a = a;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        if (exists(this.a, b, this.c)) {
            this.b = b;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        if (exists(this.a, this.b, c)) {
            this.c = c;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first side of triangle
     * @param b second side of triangle
     * @param c third side of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(double a, double b, double c) {
        return (a <= (b + c)) && (b <= (a + c)) && (c <= (a + b));
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first apex point of triangle
     * @param b second apex point of triangle
     * @param c third apex point of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(Point a, Point b, Point c) {
        return exists(a.length(b), b.length(c), a.length(c));
    }

    public double perimeter() {
        return Point.length(a, b) + Point.length(b, c) + Point.length(a, c);
    }

    public double area() {
        double halfPerimeter = perimeter() / 2.0;
        return Math.sqrt(
                halfPerimeter
                        * (halfPerimeter - Point.length(a, b)
                        * (halfPerimeter - Point.length(b, c)
                        * (halfPerimeter - Point.length(a, c))))
        );
    }
}
