public class Circle extends Figure {
    private Point center;
    private Point radiusPoint;

    public Circle() {
    }

    public Circle(Point center, Point radiusPoint) {
        this.center = center;
        this.radiusPoint = radiusPoint;
    }

    public Circle(Circle other) {
        this.center = other.center;
        this.radiusPoint = other.radiusPoint;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Point getRadiusPoint() {
        return radiusPoint;
    }

    public void setRadiusPoint(Point radiusPoint) {
        this.radiusPoint = radiusPoint;
    }

    public double radius() {
        return Point.length(center, radiusPoint);
    }

    public double perimeter() {
        return 2 * Math.PI * radius();
    }

    public double area() {
        return Math.PI * Math.pow(radius(), 2.0);
    }
}
