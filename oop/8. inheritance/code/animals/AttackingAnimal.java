/**
 * This is a base class for all attacking animals
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class AttackingAnimal extends Animal {
    private int attack;

    AttackingAnimal(double weight, int level, int attack) {
        super(weight, level); //Call to parent constructor
        this.setAttack(attack);
    }


    public int getAttack() {
        return attack;
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }
}
