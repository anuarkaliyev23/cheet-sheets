public class Cat extends Animal {
    private String name;

    public Cat(double weight, int level, String name) {
        super(weight, level);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String toString() {
        return "This is Cat! with parameters: weight = " + getWeight() + " level = " + getLevel() + " name = " + name;
    }
}
