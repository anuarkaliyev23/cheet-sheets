/**
 * This is a simple representation of an animal creature in a game
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class Animal {
    private double weight;
    private int level;

    public Animal(double weight, int level) {
        this.weight = weight;
        this.level = level;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "weight=" + weight +
                ", level=" + level +
                '}';
    }
}
