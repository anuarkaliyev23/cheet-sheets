public class Dog extends AttackingAnimal {
    private boolean isFriendly;
    private String name;

    Dog(double weight, int level, int attack, boolean isFriendly, String name) {
        super(weight, level, attack);
        this.isFriendly = isFriendly;
        this.name = name;
    }

    public boolean isFriendly() {
        return isFriendly;
    }

    public void setFriendly(boolean friendly) {
        isFriendly = friendly;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
