import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Cat(20.0, 3, "Meouwshwitz");
        System.out.println(animal);

        Animal dog = new Dog(20.0, 3, 43, false, "Rex");
        System.out.println(dog);
    }
}
