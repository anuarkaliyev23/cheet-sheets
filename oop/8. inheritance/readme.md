# Inheritance cheet sheet

This Cheet Sheet contains basic information about Inheritance in Java PL.  

Inheritance is one of the most fundamental principle of the OOP paradigm. Inheritance is a mechanism for creating heirs to classes, passing all fields and methods from "parent" to "child".  
Being not only one of the most fundamental features in all languages, providing support for OOP paradigm at any level, it also, sometimes provokes hot debates about itself.  

## Description

To show example of inheritance we will take a step further from the usual examples of different geometrical figures. To illustrate inheritance, consider the following example:  

We are making a game, in which there are several types of animals exists. They all have some level and weight. Some of them have attack power, represented in integer points. Some have names, and dogs can be friendly or not-friendle. To describe it schematically, consider following list:

1. Dog
    - weight
    - level
    - isFriendly
    - name
    - attack
2. Cat
    - weight
    - level
    - name
3. Bear
    - weight
    - level
    - attack

As we can see here, all of our classes are logically connected - they all represent some animals. Other than that, all of them have one or more repeating property. Basic idea of inheritance is to provide a mechanism to reduce repeatable boilerplate code. To do this we can try to create a separate class, unionizing all of them and **containing all fields repeated in each class**.

1. Animal
    - level
    - weight
   1. Dog
        - isFriendly
        - attack
        - name
   2. Cat
      - name
   3. Bear
      - attack

We can see, that `Dog` and `Bear` also have common property - `attack`. We can restructure it as follows:

1. Animal
    - weight
    - level
   1. Cat
        - name
   1. Attacking Animal
        - attack
      1. Dog
         - isFriendly
         - name
      1. Bear
