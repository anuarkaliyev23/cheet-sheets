# Exceptions Cheet Sheet

This Cheet Sheet contains basic information about exceptions in Java PL.

`Exception`s are mechanics for handling errors that occur during program execution. Mechanism of Exception are common in programming languages and is not applicable to java exclusively.  
Ultimately exceptions are all about errors[^error-disclaimer].

[^error-disclaimer]: "error" here used as a general term, not to be confused with `Error` in Java PL

`Exception`s are like a cannonball of sorts. They are "thrown" and kill anything on their way. Ultimately, if no one "catches" it, program will crash also.

## Logical classification

There are different ways of errors classifications. We could divide all errors occuring during programming in several rough categories:

- Compile-time
    - Error occured **before** program even started
    - Easiest to handle
    - Program won't start
- Runtime
    - Error occured **after** program started
    - Program is stopped
- Logic
    - No technical error, but program behaves not as expected
    - Hardest to handle
    - Program continues to work incorrectly

We are always trying to turn Logic errors to Runtime errors and Runtime errors in Compile-time errors.

### Compile-Time

Compile-time errors are most preferable ones. They are easiest to work with. The most common example of compile-time error is a syntax error. Such errors could be seen at compilation phase and are always obvious. In most cases, your IDE will see them even before you tried to compile your code.

Example:

`Main.java`

```java
class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World") //semicolon is omitted, will cause Exception. 
    }
}
```

### Runtime[^runtime-disclaimer]

[^runtime-disclaimer]: "Runtime" here is a general term. Not to be confused with `RuntimeException` in Java PL

Runtime errors occurs when there seemingly all ok with the code itself, but during its execution some situation caused some error to occur.

Example:

`Main.java`

```java
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            System.out.println("a / b = " + (a / b)); //Exception if b is zero
        }
    }
}
```

This code compiles and, at a first glance, there is nothing to worry about. However this code could potentially lead to errors unpreventable at compile-time. If `b` at some point will be equals to `0`, than the expression `a / b` will be impossible to compute[^zero-division].

[^zero-division]: For `int` variables in Java PL. Depends on the arithmetic system being used. In floating-point arightmetic, regulated by IEEE-754 there could be divisions to zero, producing valid result.

If we execute the code above and make `b` equals to `0`, the output will be something similar to this:

```text
2
0
Exception in thread "main" java.lang.ArithmeticException: / by zero
    at kz.mnc.cheet.sheets.vii.exceptions.Main.main(Main.java:11)

Process finished with exit code 1
```

Please note that **program execution was stopped**. This is intended purpose of any `Exception`. `Exception` that was thrown, will go like a cannonball through all sequential methods that caused it, and stop them.

> To be more precise, each time you call a method, a new `frame` is created and put on `stack` in JVM. `Exception` will traverse this stack, terminating all your `frames`.

Consider following example:

in function `main`, function `a` called. in function `a`, function `b` called. If `b` throws an exception, it will terminate `b`, then `a`, then `main`, ultimately terminating whole program.

Pseudocode:

```text
main() {
    a();
}

a() {
    b();
}

b() {
    throw new Exception();
}
```

1. main() -> a() -> b()
1. main() -> a() -> ~~b()~~
1. main() -> ~~a()~~ -> ~~b()~~
1. ~~main()~~ -> ~~a()~~ -> ~~b()~~

To prevent an `Exception`[^any-throwable-disclaimer] from terminating our entire program, we could use a `try-catch` block. More on that in the next sections

[^any-throwable-disclaimer]: Strictly speaking, any `Throwable`

### Logic Errors

Logic error is not an error in technical sense. Logic errors is anything that works not as intended/expected. They exist only in the context of specific task.  

Example:

`Main.java`

```java
class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 100; i--) { // Unintended infinite loop
            System.out.println("This intended to be printed 100 times");
        }
    }
}
```

In this example code compiles just fine. It executes, at least from technical perspective fine too. Although on code execution we can see, that loop is not executing 100 times, but rather infinitely. This is an example of logic error[^logic-error-disclaimer]

[^logic-error-disclaimer]: Not all infinite loops are logical errors. It is only if it was unexpected.

Logical errors are the hardesе to work with. First of all to determine if anything is a logical error, we must be aware of the specific task/context. They are hard to find for your IDE[^ide-static-analyzer]. In case of domain-specific task, impossible even.

[^ide-static-analyzer]: To be more precise, IDE itself is not analyzing your code. Analyzing part called `static code analyzer`

### Try-catch

We could stop an `Exception` from terminating our program with `try-catch` code block. Let's return to a previous example with division by zero:

`Main.java`

```java
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            try {
                System.out.println(a / b);
            } catch (ArithmeticException e) { 
                System.out.println("Cannot divide by zero!. Please try again:");
            }
        }
    }
}
```

if we execute the code above, and try to set `b` as `0`. We will see that program will not stop now and will print the message we programmed:

```text
2 0
Cannot divide by zero!. Please try again:
2 0
Cannot divide by zero!. Please try again:
2 0
Cannot divide by zero!. Please try again:
3 0
Cannot divide by zero!. Please try again:
5 2
2
```

All potentially dangerous code must be placed inside `try` block.  
`catch` block holds a reaction to an occurred `Exception`.

`try-catch` block could also contain `finally` block, which is not a primary topic for this cheet sheet.

## Technical classification

Although we have seen rough classification of error types, it still is classification more applicable to human comprehension. In this section we will see more programming-related classification and it will be less general and be applicable to Java PL exclusively.

All possible errors could be divided differently, depending what we will see as main dividing criteria. Logically, they can be divided as such:

1. `Throwable`
    1. `Exception`
        1. `checked`
            - Must be handled
        1. `unchecked`
            - Handling could be omitted
    1. `Error`

As you can see, one way or another, all possible errors will always be subset of `Throwable`[^throwable-disclaimer].  
`Exception`s are related to Java PL  
`Error` are caused by JVM  

`Exception`s are main topic of this cheet sheet and we will focus mainly on them.  
`Exception` can be logically divided in two categories:

- `checked`
    - `Exception` and its heirs
- `unchecked`
    - `RuntimeException` and its heirs

> Not all languages divide own `Exception` mechanics in these categories. More than that, this decision is arguably unpopular and is often a hot topic for a debate. In other languages work with exception are more similar to `unchecked` `Exception`s in Java PL

[^throwable-disclaimer]: They will be heirs. Inheritance topic will be covered in the next cheet sheets
[^checked-heir-disclaimer]: except `RuntimeException`

### Unchecked Exceptions

Unchecked exceptions are the one developer must not handle.  
To trigger an exception to occur, we must `throw` it.

Let's look at our `Point` class:

`Point.java`

```java

/**
 * Represents Geometric point in cartesian coordinates
 *
 * @author Khambar Dussaliyev
 * @since 0.1.0
 * */
public class Point {
    /**
     * x-coordinate on cartesian coordinate system
     *
     * @since 0.1.0
     * */
    private int x;

    /**
     * y-coordinate on cartesian coordinate system
     *
     * @since 0.1.0
     * */
    private int y;

    // Default constructor
    public Point() {}

    // Fully-parametrized constructor
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Copy-constructor
    public Point(Point other) {
        this.x = other.x;
        this.y = other.y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    /**
     * Calculates difference between two points on cartesian plane
     *
     * @param first first point
     * @param second second point
     *
     * @return distance between two points
     * */
    public static double length(Point first, Point second) {
        return Math.sqrt(Math.pow(second.x - first.x, 2.0) + Math.pow(second.y - first.y, 2.0));
    }

    /**
     * Difference between this and other points on cartesian plane
     *
     * @param other destination point for length calculation
     *              
     * @return distance between two points
     * */
    public double length(Point other) {
        return length(this, other);
    }
}

```

`Triangle.java`

```java
public class Triangle {
    private Point a;
    private Point b;
    private Point c;

    public Triangle() {
    }

    public Triangle(Point a, Point b, Point c) {
        if (exists(a, b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } throw new RuntimeException("Non-existent triangle");
    }

    public Triangle(Triangle other) {
        this.a = other.a;
        this.b = other.b;
        this.c = other.c;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) {
        if (exists(a, this.b, this.c)) {
            this.a = a;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) {
        if (exists(this.a, b, this.c)) {
            this.b = b;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) {
        if (exists(this.a, this.b, c)) {
            this.c = c;
        }
        else throw new RuntimeException("Non-existent triangle");
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first side of triangle
     * @param b second side of triangle
     * @param c third side of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(double a, double b, double c) {
        return a < b + c && b < a + c && c < a + b;
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first apex point of triangle
     * @param b second apex point of triangle
     * @param c third apex point of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(Point a, Point b, Point c) {
        return exists(a.length(b), b.length(c), a.length(c));
    }
}
```

Therefore our `Main.java` will look something like this:

`Main.java`

```java
public class Main {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(new Point(0,0), new Point(0, 0), new Point(0, 0)); // Non-existent triangle, throws an Exception
    }
}
```

If we will execute our code, output will be similar to this:

```text
Exception in thread "main" java.lang.RuntimeException: Non-existent triangle
 at kz.mnc.cheet.sheets.vii.exceptions.Triangle.<init>(Triangle.java:16)
 at kz.mnc.cheet.sheets.vii.exceptions.Main.main(Main.java:5)
```

This called a `stacktrace`. Ultimately, `stacktrace` is a report of all consequential method calls that lead to this `Exception` being thrown.

### Checked Exceptions

`checked Exception`s are a bit trickier. They must be handled and this is being checked at compile-time. Therefore non-handling this exception will result in compilation error.

You have 2 ways of handling `Exception`:

1. Mark method with `throws` keyword
1. Surround dangerous code with `try-catch` block

To show this in action, we will rewrite our `Triangle` example:

`Triangle.java`

```java
public class Triangle {
    private Point a;
    private Point b;
    private Point c;

    public Triangle() {
    }

    public Triangle(Point a, Point b, Point c) throws Exception { //throws clause added
        if (exists(a, b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } throw new Exception("Non-existent triangle"); //Exception instead of RuntimeException
    }

    public Triangle(Triangle other) {
        this.a = other.a;
        this.b = other.b;
        this.c = other.c;
    }

    public Point getA() {
        return a;
    }

    public void setA(Point a) throws Exception { //throws clause added
        if (exists(a, this.b, this.c)) {
            this.a = a;
        }
        else throw new Exception("Non-existent triangle"); //Exception instead of RuntimeException
    }

    public Point getB() {
        return b;
    }

    public void setB(Point b) throws Exception { //throws clause added
        if (exists(this.a, b, this.c)) {
            this.b = b;
        }
        else throw new Exception("Non-existent triangle"); //Exception instead of RuntimeException
    }

    public Point getC() {
        return c;
    }

    public void setC(Point c) throws Exception { //throws clause added
        if (exists(this.a, this.b, c)) {
            this.c = c;
        }
        else throw new Exception("Non-existent triangle"); //Exception instead of RuntimeException
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first side of triangle
     * @param b second side of triangle
     * @param c third side of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(double a, double b, double c) {
        return a < b + c && b < a + c && c < a + b;
    }

    /**
     * Determines if triangle could exist in Euclidean geometry
     *
     * @param a first apex point of triangle
     * @param b second apex point of triangle
     * @param c third apex point of triangle
     *
     * @return true if triangle could exist, false otherwise
     * */
    private boolean exists(Point a, Point b, Point c) {
        return exists(a.length(b), b.length(c), a.length(c));
    }
}
```

As you can see, we marked all the methods that might throw an exception with `throws` clause. This is somewhat of a `BE CAUTIOUS, DANGEROUS` marking. It helps other developers to quickly determine visually potentially dangerous methods.

> You must remember that you must handle checked exception not only on the method potentially throwing an `Exception`, but in all subsequent calls also. Once method calls another method with `throws` clause in signature, it became dangerous too and must be marked as such!

In this case our `Main.java` we can rewrite as such:

`Main.java`

```java
public class Main {
    public static void main(String[] args) {
        try {
            Triangle triangle = new Triangle(new Point(0,0), new Point(0, 0), new Point(0, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

Alternatively, we could do something like this also[^main-throws-disclaimer]:

```java
public class Main {
    public static void main(String[] args) throws Exception {
        Triangle triangle = new Triangle(new Point(0,0), new Point(0, 0), new Point(0, 0));
    }
}
```

[^main-throws-disclaimer]: Although being possible, this way will defeat all theorethical purpose of `checked Exception`s
